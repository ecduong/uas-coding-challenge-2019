# Server to serve GPS coordinates of aircraft
# Author: Eric Duong

import socket
import sys

# Read data from file
f = open("../data/missionwaypoints.txt", "r")
data = []
time_s = 0
for line in f:
	# Assume each line has latitude and longitude info only
	data.append(line)

# Create socket connection
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_addr = ("localhost", 12000)

print("Connecting to %s:%s" % server_addr)

sock.bind(server_addr)

while True:
	print("Waiting to receive...")

	recv_data, recv_address = sock.recvfrom(4096)
	if time_s < len(data):
		sock.sendto(data[time_s].encode(), recv_address)
		
		# Simulate new data that would appear after 1 second
		time_s = time_s + 1
	else:
		sock.sendto(''.encode(), recv_address)
		break

sock.close()
