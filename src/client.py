# Client to connect to server and based on results, calculate (average) aircraft speed
# Author: Eric Duong

import math
import socket
import sys
import time

from pyproj import Proj

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_addr = ("localhost", 12000)

# Assume data is confined to zone 10 in the northern hemisphere
proj = Proj(proj="utm", zone="10U", ellps="WGS84")

prev_x = 0
prev_y = 0
speed_sum = 0
speed_count = 0

while True:
	sock.sendto(''.encode(), server_addr)
	data, server = sock.recvfrom(4096)
	lat_long = data.decode().split()

	if len(lat_long) > 0:
		latitude = float(lat_long[0])
		longitude = float(lat_long[1])

		# Convert to UTM
		utm_x, utm_y = proj(longitude, latitude)

		# Calculate distance travelled in 1s
		if prev_x != 0 and prev_y != 0:
			delta_x = utm_x - prev_x
			delta_y = utm_y - prev_y
			speed = math.sqrt(delta_x ** 2 + delta_y ** 2)

			print("Current speed: %.2f m/s" % speed )

			speed_sum = speed_sum + speed
			speed_count = speed_count + 1

		prev_x = utm_x
		prev_y = utm_y

		# Simulate 1 second delay between readings
		time.sleep(1)
	else:
		if speed_count > 0:
			avg_speed = speed_sum / speed_count
			print("\nAverage speed: %.2f m/s" % avg_speed)
		break

sock.close()