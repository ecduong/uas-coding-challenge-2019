# UAS Coding Challenge 2019
Given the data provided, this program calculates the current speed of the aircraft in m/s as well as the cumulative average speed in m/s. For the purposes of the challenge, the code makes the following assumptions:
- Each line in the data file `missionwaypoints.txt` contains a latitude and longitude coordinate, separated by a space
- All coordinates are in UTM zone 10U

## Prerequisites
- Python 3.x
- pyproj ([installation instructions](https://pyproj4.github.io/pyproj/v2.2.0rel/installation.html))

## Usage
### Start the server
1. Open a Python console such as Anaconda
2. `cd` into the `uas-coding-challenge-2019/src` directory
3. Run `server.py` by using the command `python server.py`

### Run the client
1. Open a new Python console tab or window
2. `cd` into the `uas-coding-challenge-2019/src` directory
3. Run `client.py` by using the command `python client.py`


